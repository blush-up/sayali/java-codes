/*
 * A b C d E f G h I j
 * */
class Ten {
	public static void main(String[] args) {
		char ch = 'A';
		for(int i=1;i<=10;i++){
			if(ch%2==1){
				System.out.print(ch +" ");
			}else{
				System.out.print((char)(ch+32) +" ");
			}
			ch++;
		}
	}
}
