/*
 * 9 B 7 D 5 F 3 H 1 
 * */
class Nine {
	public static void main(String[] args) {
		char ch = 'A';
		for(int i=9;i>=1;i--){
			if(i%2==0){
				System.out.print(ch +" ");
			}else{
				System.out.print(i +" ");
			}
			ch++;
		}
	}
}
