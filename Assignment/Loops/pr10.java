 class C2W  {
    public static void main(String[] args) {
        System.out.println("Counting from 1 to 10 using a for loop:");
        for (int i = 1; i <= 10; i++) {
            System.out.print(i + " ");
        }
        System.out.println();

        System.out.println("Counting from 1 to 10 using a while loop:");
        int count = 1;
        while (count <= 10) {
            System.out.print(count + " ");
            count++;
        }
        System.out.println();
    }
}

