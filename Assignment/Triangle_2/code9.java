/*
      4
      4 a
      4 b 6
*/
import java.io.*;
class Nine {
        public static void main(String[] args)throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                int row = Integer.parseInt(br.readLine());
                char ch ='a';
		for(int i=1;i<=row;i++){
                        for(int j=1;j<=i;j++){
                                if(j%2==0){
                                        System.out.print(ch++ +" ");
                                }else{
                                        System.out.print(j+row +" ");
                        	}
                        }
                        System.out.println();
                }
        }
}
