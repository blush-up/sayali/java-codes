/*
      c
      C B
      c b a
*/
import java.io.*;
class Four {
        public static void main(String[] args)throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                int row = Integer.parseInt(br.readLine());
                for(int i=1;i<=row;i++){
                        int num1 = row+64;
			int num2 = row+96;
                        for(int j=1;j<=i;j++){
                                if(i%2==1){
                                        System.out.print((char)num2 +" ");
					num2--;
                                }else{
                                        System.out.print((char)num1 +" ");
					num1--;
                        }
                        }
                        System.out.println();
                }
        }
}
