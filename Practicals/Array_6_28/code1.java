import java.util.*;
class One{
	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Size of array");
		int size = sc.nextInt();
		int arr[] = new int [size];
		int flag = 0;
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int num = arr[0];
		for(int i=1;i<arr.length;i++){
			if(arr[i]<num){
				num=arr[i];
			}else{
				flag++;
			}
		}
		if(flag==0){
			System.out.println("Array in descending order");
		}else{
			System.out.println("Array is not descending");
		}
		
	}
}
