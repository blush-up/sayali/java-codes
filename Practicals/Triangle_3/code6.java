import java.util.*;
class Six {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                int row = sc.nextInt();
                for(int i=1;i<=row;i++){
                        int num = 1;
                        for(int j=row;j>=i;j--){
                                if(j%2==0){
					System.out.print(num+" ");
                                }else{
					System.out.print((char)(num+96) + " ");
					num++;
                                }
                                
                        }
                        System.out.println();
                }

        }
}
