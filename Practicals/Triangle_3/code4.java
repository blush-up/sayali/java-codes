import java.util.*;
class Four {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                int row = sc.nextInt();
                int num = row*(row+1)/2;
                for(int i=1;i<=row;i++){
                        for(int j=row;j>=i;j--){
                                System.out.print((char)(num+64) + " ");
                                num--;
                        }
                        System.out.println();
                }

        }
}
