import java.util.*;
class Two{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter the size of the Array");
                int size = sc.nextInt();
                int arr[] = new int[size];
                for(int i=0;i<arr.length;i++){
                        arr[i] = sc.nextInt();
                }
                int sumE = 0,sumO = 0;
                for(int i=0;i<arr.length;i++){
                        if(arr[i]%2==0){
                                sumE=sumE+arr[i];
                        }else if(arr[i]%2==1){
				sumO=sumO+arr[i];
			}
                }
                System.out.println("even number sum is :"+ sumE);
		System.out.println("odd number sum is :"+ sumO);
        }
}
