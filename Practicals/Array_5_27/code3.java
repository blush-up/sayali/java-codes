import java.util.*;
class Three{
        public static void main(String[] args){

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter the size of the Array");
                int size = sc.nextInt();
                int arr[] = new int[size];
                for(int i=0;i<arr.length;i++){
                        arr[i] = sc.nextInt();
                }
                int cnt = 0; 
                for(int i=0;i<arr.length/2;i++){
                       if(arr[i]==arr[size-i-1]){
		       		cnt++;
		       } 
                }
		if(cnt==0){
			System.out.println("Not Palindrome");
		}else{
			System.out.println("Palindrome");
		}
        }
}
