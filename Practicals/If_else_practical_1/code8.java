class Div{
	public static void main(String[] args){
		int num = 120;
		if(num%2==0 && num%5==0 && num%10==0){
			System.out.println("number is divisible by 2,5,10");
		}else if(num%2==0){
			System.out.println("number is divisible by 2");
		}else if(num%5==0){
			System.out.println("number is divisible by 5");
		}else if(num%10==0){
			System.out.println("number is divisible by 10");
		}else{
			System.out.println("number is  neither divisible by 2,5,10");
		}
	}
}
