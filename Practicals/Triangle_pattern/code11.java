/*
     D C B A
     D C B
     D C
     D

     */
import java.io.*;
class Eleven {
        public static void main(String[] args)throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("enter the rows :");
                int row = Integer.parseInt(br.readLine());
                	for(int i=1;i<=row;i++){
                        	int num = 64+row;
                        	for(int j=row;j>=i;j--){
                                	System.out.print((char)num + " ");
					num--;
				}
                        	System.out.println();
                }
        }
}
