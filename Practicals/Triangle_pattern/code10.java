/*
     1 2 3 4
     2 3 4
     3 4
     4

     */
import java.io.*;
class Ten {
        public static void main(String[] args)throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("enter the rows :");
                int row = Integer.parseInt(br.readLine());

                for(int i=1;i<=row;i++){
                        int num = i;
                        for(int j=row;j>=i;j--){
                                System.out.print(num++ + " ");
                        }

                        System.out.println();
                }
        }
}
