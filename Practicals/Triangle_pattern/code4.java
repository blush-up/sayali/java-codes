/*
     A 
     B C
     C D E

     */
import java.io.*;
class Four {
        public static void main(String[] args)throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("enter the rows :");
                int row = Integer.parseInt(br.readLine());
                int num =64;
                for(int i=1;i<=row;i++){
			num = i+64;
                        for(int j=1;j<=i;j++){
                                System.out.print((char)num++ + " ");
                        }
                        System.out.println();
                }
        }
}
