import java.util.*;
class One {
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the array");
		int size = sc.nextInt();
		int arr[] = new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int sum = 0;
		int avg = 0;
		for(int i=0;i<arr.length;i++){
			sum=sum+arr[i];
		}
		avg=sum/size;
		System.out.println("Average of element in array "+avg);
	}	
}
