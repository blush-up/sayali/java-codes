import java.util.*;
class Five {
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter the size of the array");
                int size = sc.nextInt();
                int arr[] = new int[size];
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.nextInt();
                }
                int temp = arr[0];
                for(int i=0;i<arr.length/2;i++){
                        temp=arr[i];
			arr[i]=arr[size-1-i];
			arr[size-1-i]=temp;
                }
		System.out.println("Reverse of array is :");
                for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
        }
}
