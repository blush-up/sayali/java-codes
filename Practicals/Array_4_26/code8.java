import java.util.*;
class Eight {
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter the size of the array");
                int size = sc.nextInt();
                char arr[] = new char[size];
                for(int i=0;i<arr.length;i++){
                        arr[i]=sc.next().charAt(0);
                }
                System.out.println("Enter the character you want to find their occurance in array");
                char ch = sc.next().charAt(0);
                int cnt = 0;
                for(int i=0;i<arr.length;i++){
                        if(arr[i]==ch){
                                cnt++;
                        }
                }
                System.out.println(ch + " occurs " + cnt + " in th array ");
        }
}
