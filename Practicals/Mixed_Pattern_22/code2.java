import java.util.*;
class Two{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter the number of rows");
                int row = sc.nextInt();
                int num = 1;
                for(int i=1;i<=row;i++){
                        for(int spc=1;spc<i;spc++){
                                System.out.print(" " + "\t");
                        }
                        for(int j=row;j>=i;j--){
                                System.out.print(num + "\t");
                                num++;
                        }
			num--;
                        System.out.println();

                }


        }
}
