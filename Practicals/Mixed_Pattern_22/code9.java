import java.util.*;
class Nine{
        public static void main(String[] args){
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter the number of rows");
                int row = sc.nextInt();
                int space = 0;
                int col = 0;
                for(int i=1;i<=row;i++){
                        if(i<=row){
                                space=row-i;
                                col=2*i-1;
                        }
                        for(int spc=1;spc<=space;spc++){
                                System.out.print(" " + "\t");
                        }
                        int num=row-1+i;
                        for(int j=1;j<=col;j++){

                                        System.out.print(num-- +"\t");
                        }

                        System.out.println();

                }


        }
}
