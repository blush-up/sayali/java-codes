import java.util.*;
class One{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows");
		int row = sc.nextInt();
		int num = 1;
		for(int i=1;i<=row;i++){
			for(int spc=1;spc<=row-i;spc++){
				System.out.print(" " +"\t");
			}
			for(int j=1;j<=i;j++){
				System.out.print(num + "\t");
				num+=2;
			}
			System.out.println();
		
		}

	
	}
}
