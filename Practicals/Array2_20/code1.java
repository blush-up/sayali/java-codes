import java.util.*;
class One{
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of the array");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter the element of the array");
		for(int i=0;i<size;i++){

			arr[i]=sc.nextInt();
		}
		int cnt = 0;
		for(int i=0;i<size;i++){
			
			if(arr[i]%2==0){
				System.out.print(arr[i] +", ");
				cnt++;
			}
			
		}
		System.out.println();
		System.out.println("count of the even numbers is :"+cnt);
		
	}
}
