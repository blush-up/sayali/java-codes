import java.util.*;
class Six{
        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter the size of the array");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Enter the element of the array");
                for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
                }
                int pro = 1;
                for(int i=0;i<size;i++){

                        if(i%2==1){
                                pro = pro*arr[i];
                        }

                }
                System.out.println("Product of the odd indexed element:" + pro);

        }
}
