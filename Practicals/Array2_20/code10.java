import java.util.*;
class Ten{
        public static void main(String[] args) {

                Scanner sc = new Scanner(System.in);
                System.out.println("Enter the size of the array");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Enter the element of the array");
                for(int i=0;i<size;i++){

                        arr[i]=sc.nextInt();
                }
                int temp = arr[0];
		int index = 0;
                for(int i=0;i<size;i++){

                        if(temp<arr[i]){
                                temp = arr[i];
				index = i;
                        }

                }
                System.out.println("maximum element in array is at pos " + index + " is " + temp);

        }
}
