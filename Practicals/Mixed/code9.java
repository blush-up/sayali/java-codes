import java.util.*;
class Nine {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                int row = sc.nextInt();
                for(int i=1;i<=row;i++){
                	int ch = 64 + row - i + 1;
                        for(int j=1;j<=row-i+1;j++){
                                if(i%2==1){
					System.out.print(j + " ");
                        	}else{
					System.out.print((char)ch +" ");
					ch--;
				}
			}
                        System.out.println();
                }

        }
}
