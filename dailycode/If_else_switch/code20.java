class Twenty {
	public static void main (String[] args) {
		String friend = "Kanha";
		System.out.println("Before Switch");
		switch(friend){
			case "Ashish" :
				System.out.println("Barclayes");
				break;
			case "Kanha" :
                                System.out.println("BMC software");
                                break;
			case "Rahul" :
                                System.out.println("Infosys");
                                break;
			case "Badhe" :
                                System.out.println("IBM");
                                break;
			default :
				System.out.println("in default case");
		}
		System.out.println("After Switch");
	}
}
